﻿using System.ComponentModel.DataAnnotations;

namespace KodSynaMaminojPodrugi.Enums
{
    internal enum MenuPoint
    {
        [Display(Name = "StartMenuPoint")]
        Start = 0,

        [Display(Name = "SettingsMenuPoint")]
        Settings,

        [Display(Name = "RulesMenuPoint")]
        Rules
    }
}
