﻿using System;

namespace KodSynaMaminojPodrugi.Exceptions
{
    /// <summary>
    /// The exception that is thrown when the user's input is not valid
    /// </summary>
    public class InvalidInputException : Exception
    {
        public InvalidInputException()
        {

        }

        public InvalidInputException(string message): base(message)
        {

        }
    }
}