﻿namespace KodSynaMaminojPodrugi.Services.Contracts
{
    internal interface ISettingService
    {
        /// <summary>
        /// Saves a config
        /// </summary>
        /// <param name="key">config key</param>
        /// <param name="value">config value</param>
        void SetSetting(string key, object value);

        /// <summary>
        /// Returns config value by key
        /// </summary>
        /// <param name="key">config key</param>
        /// <returns></returns>
        object GetSetting(string key);

        /// <summary>
        /// Returns config value by key
        /// </summary>
        /// <param name="key">config key</param>
        /// <returns></returns>
        T GetSetting<T>(string key);
    }
}
