﻿using KodSynaMaminojPodrugi.Models;
using KodSynaMaminojPodrugi.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;

namespace KodSynaMaminojPodrugi.Services
{
    class GameService : IGameService
    {
        private List<Player> _players = new List<Player>();
        private string _mainWord;

        /// <inheritdoc />
        public IGameService AddPlayer(string name)
        {
            if (_players.Any(p => p.Equals(name)))
                throw new InvalidOperationException("PlayerExists");

            _players.Add(new Player(name, 0));

            return this;
        }

        /// <inheritdoc />
        public string GetFormattedRulesString()
        {
            throw new System.NotImplementedException();
        }

        /// <inheritdoc />
        public int GetPlayerScore(string playerName)
        {
            throw new System.NotImplementedException();
        }

        /// <inheritdoc />
        public bool HandlePlayerInput(string playerName, string input)
        {
            throw new System.NotImplementedException();
        }

        /// <inheritdoc />
        public IGameService SetMainWord(string mainWord)
        {
            if (string.IsNullOrEmpty(mainWord))
                throw new InvalidOperationException("EmptyMainWord");

            _mainWord = mainWord;

            return this;
        }
    }
}
