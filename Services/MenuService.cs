﻿using KodSynaMaminojPodrugi.Enums;
using KodSynaMaminojPodrugi.Extensions;
using KodSynaMaminojPodrugi.Models;
using KodSynaMaminojPodrugi.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Resources;

namespace KodSynaMaminojPodrugi.Services
{
    internal class MenuService: IMenuService
    {
        private readonly IGameService _gameService;
        private readonly ISettingService _settingService;

        public MenuService(IGameService gameService, ISettingService settingService)
        {
            _gameService = gameService;
            _settingService = settingService;
        }

        /// <inheritdoc />
        public List<NameNumber> GetMenuPoints(ResourceManager resourceManager = null)
        {
            var list = new List<NameNumber>();

            foreach (MenuPoint point in Enum.GetValues(typeof(MenuPoint)))
            {
                var name = point.GetName();

                list.Add(new NameNumber(resourceManager?.GetString(name) ?? name, (int)point));
            }

            return list;
        }
    }
}
